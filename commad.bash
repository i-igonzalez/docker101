# Curso Docker 101

# Información sobre docker 

docker info

# Comprobar que Docker esta instalado y funcionando
docker --version

# Comprobar que funciona correctamente

docker run hello-world

# listar imágenes

docker image ls 

# o

docker images


# Ejemplo

cd example

docker build -t prueba:curso .

# comprobación que la imagen se ha creado

docker images

# run

docker run -d --name prueba -p 4000:80 prueba:hello 

# -d : ejecución en segundo plano
# --name: nombre del docker
# -p: puerto puerto que se expone: puerto expuesto en el docker

# comprabar si esta corriendo 

curl http://localhost:4000

# listar containers corriendo

docker container ls
docker ps 

# acceder a los logs

docker logs prueba

# listar containers incluso los que no estan corriendo

docker container ls -a
docker ps -a

# Parar el contenedor 

docker stop prueba

docker container stop prueba

cd ..

# Docker-compose 

# Docker-compose versión

docker-compose --version

# Ejemplo

cd example-compose

# Lanzar docker-compose

docker-compose up

# Acceder a la aplicación 

curl http://0.0.0.0:5000/


# Lanzar docker-compose en segundo plano

docker-compose up -d

# Listar los docker-compose

docker-compose ps

# Logs

docker-compose logs

# Parar el docker-compose

docker-compose stop

# Parar el docker-compose y borrar datos efímeros

docker-compose down

# Parar los contenedores

docker-compose down


cd ..

# Ejemplo wordpress

cd example-wordpress

docker-compose up -d

# Se ha levantado

http://localhost:8000

# Eliminar wordpress

docker-compose down

docker-compose down --volumes


# example docker hub

cd example

docker build -t web-flask:dev

docker login

docker tag image $USER_DOCKER/image

docker push $USER_DOCKER/image

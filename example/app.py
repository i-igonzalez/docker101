from flask import Flask
import os
import socket

# Connect to Redis
app = Flask(__name__)

@app.route("/")
def hello():
    html = "<h3>Hola {name}!</h3>" \
           "<b>Nombre:</b> {hostname}<br/>" 
    return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname())

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)